# Waste Manager

## Contexte

Afin d'aider le commune de Simplon les Deux Châteaux à améliorer leur gestion des déchets, un logiciel à été concu afin de gérer les déchets générés par les habitants tout en tenant compte des services de traitements des déchets disponibles.

Les types de déchets (non exhaustifs) :
* Déchets gris, le déchet de base
* Déchets plastiques, qui seront de différents types
* Déchets cartons
* Déchets organiques

Les services de traitements des déchets potentiellement disponibles :
* Incinérateur, qui accepte tout type de déchets
* Centre de tri des déchets recyclables ou compostables, se charge de trier les déchets pour les envoyer aux centres de traitements adequat.
* Centre de recyclage, peuvent traiter carton et/ou plastiques, potentiellement selon leur type
* Composteurs de quartier, peuvent traiter les déchets compostables

Les poids des déchets et les capacités de traitement sont en tonne.

**Le systeme de gestion des déchets est concu pour minimiser les émissions de CO2.**

## Contrainte technique

- Programmation orientée objet.
- Minimisation des émissions de CO2 en favorisant des methodes de traitement à faible émission.

##### Le programme doit être est en mesure de gérer les cas suivants :

- Quand le(s) centre(s) de tri est/sont arrivé(s) à la capacité maximale, tous les déchets restants sont envoyés à l’incinérateur.

- Quand les systèmes de traitements (type recycleur ou composteur) sont arrivés à la capacité maximale, tous les déchets restants sont envoyés à l’incinérateur.

- Quand le(s) incinérateur(s) sont arrivés à la capacité maximale, tous les déchets restants sont laissés en l'état.

- Quand un déchet a une masse supérieure à la capacité de traitement d'un système de traitement mais que le système de traitement a encore la possibilité de traité une partie du déchet, alors le dit déchet est coupé en deux pour que la partie traitable soit traité.

#### Emission de CO2 par service de traitement de déchets :

Ci-dessous des valeurs factices de CO2 rejettés pour les différents déchets via leur mode de traitement, expimés en kg de CO2/tonne de déchet traité :

|                 | Incinération | Recyclage | Compostage |
| --------------- | ------------ | --------- | ---------- |
| Gris (ou autre) | 30           |           |            |
| Plastique PET   | 40           | 8         |            |
| Plastique PVC   | 38           | 12        |            |
| Plastique PC    | 42           | 10        |            |
| Plastique PEHD  | 35           | 11        |            |
| Carton          | 25           | 5         |            |
| Organique       | 28           |           | 1          |
| Métaux          | 50           | 7         |            |
| Verre           | 50           | 6         |            |

#### Diagramme de classe :

![diagramme](/conception/uml.png)

## Captures d'écrans

#### Ordinateur de bureau :

![waste-manager1](/conception/waste-manager1.png)
![waste-manager2](/conception/waste-manager2.png)
![waste-manager3](/conception/waste-manager3.png)

#### Smartphone :

![waste-manager4](/conception/waste-manager4.png)
![waste-manager5](/conception/waste-manager5.png)
![waste-manager6](/conception/waste-manager6.png)

## API

#### Routes disponible :

- `/api/` 

    Requête de type POST, le corps de la requête doit être un JSON valide, ce dernier va renvoyer le résultat du traitement des déchets sous un format JSON.

    Exemple :

```json
{
	"quartiers": [
		{
			"population": 100,
			"organique": 100
		}
	],
	"services": [
		{
			"type": "incinerateur",
			"ligneFour": 1,
			"capaciteLigne": 25
		},
		{
			"type": "centreTri",
			"capacite": 10000
		},
		{
			"type": "composteur",
			"capacite": 25,
			"foyers": 1
		}
		
	]
}
```

  Va retourner :

```json
{
  "response": {
    "state": "success",
    "dechets": 2,
    "dechetsTraites": 1,
    "sousDechetsTraites": 1,
    "dechetsNonTraites": 0,
    "sousDechetsNonTraites": 1,
    "totalCO2Emis": 0.7250000000000001
  },
  "wastes": [
    {
      "subWaste": false,
      "type": "organique",
      "subType": null,
      "destroyed": true,
      "CO2Issued": 0.025,
      "mass": 25,
      "handlerName": "Composter"
    },
    {
      "subWaste": true,
      "type": "organique",
      "subType": null,
      "destroyed": true,
      "CO2Issued": 0.7000000000000001,
      "mass": 25,
      "handlerName": "Incinerator"
    },
    {
      "subWaste": true,
      "type": "organique",
      "subType": null,
      "destroyed": false,
      "CO2Issued": null,
      "mass": 50,
      "handlerName": "Untreated"
    }
  ]
}
```

## Fonctionnement

Pour lancer le programme deux possibilitées :

#### Avec Docker 😀 :

Programmes pré-requis :

  - Docker >=19
  - docker-compose >= 1.21.0
  - GNU bash >= 3.2

Placez vous à la racine du project et exécutez le fichier `start.bash`, pour arrêter exécutez `stop.bash` .

#### Sans Docker 😞 :

Programmes pré-requis :

  - PHP >=7.4 (avec rewrite activé)
  - mariaDB >= 10.2
  - Composer >= 2.0.0
  - symfony-cli => 4.15.0

Éditer le fichier d'environnement local `env.local` suivant les paramètres de connexion du programme de gestion de base de données installée.

Placez vous à la racine du project et exécutez les commandes suivantes :

1. `composer install`
2. `php bin/console doctrine:database:create -n --if-not-exists`
3. `php bin/console doctrine:migrations:migrate -n`
4. `symfony server:start`