<?php

namespace App\Controller;

use App\Business\ReportManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Business\WasteManager;
use Doctrine\ORM\EntityManagerInterface;

class APIController extends AbstractController
{
    /**
     * @Route("/api/", name="api_index", methods={"POST"})
     */
    public function api_index(Request $request, EntityManagerInterface $manager)
    {
        if ($content = $request->getContent()) {

            // Récuperation des données envoyés en JSON.
            $input = json_decode($content, true);

            if ($input) {

                // Passe dans l'object WasteManager l'ensemble des variables d'émission de CO2 par type de traîtement.
                $wasteManager = new WasteManager(
                    $input,
                    [
                        $this->getParameter('incinerator_emission'),
                        $this->getParameter('recycling_emission'),
                        $this->getParameter('composter_emission')
                    ]
                );

                // Exécution du coeur de la logique métier.
                $wasteManager->createHandlers();
                $wasteManager->createWastes();
                $wasteManager->sortingWastes();

                // Formatage de la réponse au format JSON.
                $response = array(
                    'state' => "success",
                    'dechets' => $wasteManager->getTotalWastes(),
                    'dechets' => $wasteManager->getTotalSubWastes(),
                    'dechetsTraites' => $wasteManager->getTotalWastesTreated(),
                    'sousDechetsTraites' => $wasteManager->getTotalSubWastesTreated(),
                    'dechetsNonTraites' => ($wasteManager->getTotalWastes() - $wasteManager->getTotalWastesTreated()),
                    'sousDechetsNonTraites' => ($wasteManager->getTotalSubWastes() - $wasteManager->getTotalSubWastesTreated()),
                    'totalCO2Emis' => $wasteManager->getTotalCO2Issued(),
                );

                // Récupération de l'ensemble des déchets et formatage au format JSON.
                $wastes = $wasteManager->getWastesArray();

                // Assemblage de la réponse et de la liste des déchets.
                $json = array(
                    'response' => $response,
                    'wastes' => $wastes,
                );

                // Création et persistance du rapport de traîtement des déchets.
                $report = new ReportManager();
                $report->create($wasteManager);
                $report->persist($manager);

                return new JsonResponse($json);
            } else {
                return new JsonResponse(['response' =>
                [
                    "state" => "error"
                ]]);
            }
        }

        return new JsonResponse(['response' =>
        [
            "state" => "error"
        ]]);
    }
}
