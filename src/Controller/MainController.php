<?php

namespace App\Controller;

use App\Business\ReportManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Business\WasteManager;
use App\Form\UploadType;
use App\Repository\ReportRepository;
use CMEN\GoogleChartsBundle\GoogleCharts\Charts\PieChart;
use Doctrine\ORM\EntityManagerInterface;

class MainController extends AbstractController
{
    /**
     * @Route("/send", name="send_report")
     */
    public function send_report(Request $request, EntityManagerInterface $manager)
    {
        $form = $this->createForm(UploadType::class);
        $form->handleRequest($request);

        // Si la validation du formulaire d'upload est envoyé et valide.
        if ($form->isSubmitted() && $form->isValid()) {
            $input = $form->getData();
            $input = json_decode(file_get_contents($input["file"]), TRUE);

            if ($input) {

                // Passe dans l'object WasteManager l'ensemble des variables d'émission de CO2 par type de traîtement.
                $wasteManager = new WasteManager(
                    $input,
                    [
                        $this->getParameter('incinerator_emission'),
                        $this->getParameter('recycling_emission'),
                        $this->getParameter('composter_emission')
                    ]
                );

                // Exécution du coeur de la logique métier.
                $wasteManager->createHandlers();
                $wasteManager->createWastes();
                $wasteManager->sortingWastes();

                // Création et persistance du rapport de traîtement des déchets.
                $report = new ReportManager();
                $report->create($wasteManager);
                $id = $report->persist($manager);

                return $this->redirectToRoute('show_report', ['id' => $id]);
            }
        }

        // Rendu du formulaire d'envoi de fichier.
        return $this->render('send_file.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/show/{id}", name="show_report")
     */
    public function show_report(int $id, ReportManager $report, ReportRepository $reportRepo)
    {

        // Récuperation des tableaux de données pour création des graphiques et créations des deux graphiques.
        [$handlersByWastes,$handlersCO2Issued] = $report->getChartResume($id, $reportRepo);
        
        $handlersByWastesChart = new PieChart();
        $handlersByWastesChart->getData()->setArrayToDataTable($handlersByWastes);

        $handlersCO2IssuedChart = new PieChart();
        $handlersCO2IssuedChart->getData()->setArrayToDataTable($handlersCO2Issued);

        // Rendu des deux graphiques et du rapport.
        return $this->render('show_report.html.twig', [
            'report' => $report->get($id, $reportRepo),
            'handlersByWastes' => $handlersByWastesChart,
            'handlersCO2IssuedChart' => $handlersCO2IssuedChart
        ]);
    }

    /**
     * @Route("/", name="list_report")
     */
    public function list_report(ReportManager $report, ReportRepository $reportRepo)
    {
        // Rendu da la liste des rapports.
        return $this->render('list_report.html.twig', [
            'reports' => $report->getAll($reportRepo),
        ]);
    }
}
