<?php

namespace App\Services;

use App\Business\Waste;

class ComposterHandler implements HandlerInterface
{

    private int $capacityMax;
    private int $family;
    private float $CO2Issued = 0;
    private int $wasteHandled = 0;
    private array $paramsEmission;

    function __construct(int $capacityMax, int $family, array $paramsEmission)
    {
        $this->capacityMax = $capacityMax;
        $this->family = $family;
        $this->paramsEmission = $paramsEmission;
    }

    // Retourne la CO2 qui serait émis si cette methode de traîtement était utilisé.
    public function getCO2AfterTreatment(Waste $waste): float
    {
        return $this->paramsEmission[$waste->getType()] / 1000 * $waste->getMass();
    }

    // Retourne le CO2 émis dû au traîtement du déchet.
    public function getCO2Issued(): float
    {
        return $this->CO2Issued;
    }

    // Permet de tester si la méthode de traîtement est disponible suivant le déchet.
    public function canHandle(Waste $waste): bool
    {
        if ("organique" == $waste->getType()) {

            if ($this->capacityMax > 0) {
                return true;
            }
            return false;
        }
        return false;
    }

    // Compostage du déchet et création d'un sous déchet quand la masse du déchet à composter et supperieur à la masse de traîtement possible.
    public function handle(Waste $waste, $createWaste): void
    {
        if ($this->canHandle($waste)) {
            if (($waste->getMass() - $this->capacityMax) > 0) {

                $createWaste(($waste->getMass() - $this->capacityMax), $waste->getType(), null, true);
                $waste->setMass($this->capacityMax);
            }

            $waste->destroy();
            $waste->setCO2issued($this->getCO2AfterTreatment($waste));
            $waste->setHandlerType(str_replace('Handler', '', explode('\\', get_class($this))[2]));
            $this->wasteHandled++;
            $this->capacityMax -= $waste->getMass();
        }
    }
}
