<?php

namespace App\Services;

use App\Business\Waste;

class IncineratorHandler implements HandlerInterface
{

    private int $capacityMax;
    private float $CO2Issued = 0;
    private int $wasteHandled = 0;
    private array $paramsEmission;


    function __construct(int $ovenLineMax, int $capacityLineMax, array $paramsEmission)
    {
        $this->capacityMax = $ovenLineMax * $capacityLineMax;
        $this->paramsEmission = $paramsEmission;
    }

    // Retourne la CO2 qui serait émis si cette methode de traîtement était utilisé.
    public function getCO2AfterTreatment(Waste $waste): float
    {
        if ($waste->getSubType() !== null) {

            return current(array_column($this->paramsEmission[$waste->getType()], current([$waste->getSubType()]))) / 1000 * $waste->getMass();
        } else {
            return $this->paramsEmission[$waste->getType()] / 1000 * $waste->getMass();
        }
    }

    // Retourne le CO2 émis dû au traîtement du déchet.
    public function getCO2Issued(): float
    {
        return $this->CO2Issued;
    }

    // Permet de tester si la méthode de traîtement est disponible suivant le déchet.
    public function canHandle(Waste $waste): bool
    {
        if ($this->capacityMax > 0) {
            return true;
        }
        return false;
    }

    // Incinération du déchet et création d'un sous déchet quand la masse du déchet à icinérer et supperieur à la masse de traîtement possible.
    public function handle(Waste $waste, $createWaste): void
    {
        if ($this->canHandle($waste)) {
            if (($waste->getMass() - $this->capacityMax) > 0) {

                $createWaste(($waste->getMass() - $this->capacityMax), $waste->getType(), $waste->getSubType(), true);
                $waste->setMass($this->capacityMax);
            }

            $waste->destroy();
            $waste->setCO2issued($this->getCO2AfterTreatment($waste));
            $waste->setHandlerType(str_replace('Handler', '', explode('\\', get_class($this))[2]));
            $this->wasteHandled++;
            $this->capacityMax -= $waste->getMass();
        }
    }
}
