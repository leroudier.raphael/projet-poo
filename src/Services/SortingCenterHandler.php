<?php

namespace App\Services;

use App\Business\Waste;

class SortingCenterHandler implements SortingCenterHandlerInterface
{

    private int $capacityMax;

    function __construct(int $capacityMax)
    {
        $this->capacityMax = $capacityMax;
    }

    // Permet de trouver la méthode de traîtement émettant le moins de CO2.
    private function findWeakestCO2Handler(Waste $waste, array $wastesHandlers): int
    {
        $CO2AfterTreatment = 9999999999;
        $handlersIterator = 0;

        for ($i = 0; $i < count($wastesHandlers); $i++) {

            if ($wastesHandlers[$i]->canHandle($waste)) {
                if ($wastesHandlers[$i]->getCO2AfterTreatment($waste) < $CO2AfterTreatment) {
                    $CO2AfterTreatment = $wastesHandlers[$i]->getCO2AfterTreatment($waste);
                    $handlersIterator = $i;
                }
            }
        }

        return $handlersIterator;
    }

    // Permet de tester si la méthode de traîtement est disponible suivant le déchet.
    public function canHandle(Waste $waste): bool
    {
        if ($this->capacityMax > 0) {
            return true;
        }
        return false;
    }

    // Envoi du déchet au système de traîtement émettant le moins de CO2 et création d'un sous déchet quand la masse du déchet à trier et supperieur à la masse de traitement possible.
    public function handle(Waste $waste, array $wastesHandlers, $createWaste): void
    {
        if ($this->canHandle($waste)) {
            if (($waste->getMass() - $this->capacityMax) > 0) {

                $createWaste(($waste->getMass() - $this->capacityMax), $waste->getType(), $waste->getSubType(), true);
                $waste->setMass($this->capacityMax);
            }

            $handlersIterator = $this->findWeakestCO2Handler($waste, $wastesHandlers);
            $wastesHandlers[$handlersIterator]->handle(
                $waste,
                function (float $mass, string $type, ?string $subType = null, ?bool $subWaste = false)  use ($createWaste) {
                    $createWaste($mass,  $type,  $subType, $subWaste);
                }
            );

            $this->capacityMax -= $waste->getMass();
        }
    }
}
