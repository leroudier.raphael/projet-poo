<?php

namespace App\Services;

use App\Business\Waste;

class RecyclingCenterHandler implements HandlerInterface
{

    private int $capacityMax;
    private string $type;
    private array $subType;
    private bool $deposit;
    private int $wasteHandled = 0;
    private float $CO2Issued = 0;
    private array $paramsEmission;

    function __construct(int $capacityMax, string $type, array $subType = NULL, ?bool $deposit = NULL, array $paramsEmission)
    {
        $this->capacityMax = $capacityMax;
        $this->type = $type;
        $this->subType = $subType;
        $this->deposit = $deposit;
        $this->paramsEmission = $paramsEmission;

        $this->transformTypeName();
    }

    // Permet de ne garder que le type recycleur.
    private function transformTypeName(): void
    {
        $this->type = strtolower(str_replace("recyclage", "", $this->type));
    }

    // Retourne la CO2 qui serait émis si cette methode de traîtement était utilisé.
    public function getCO2AfterTreatment(Waste $waste): float
    {
        if ($waste->getSubType() !== null) {

            return current(array_column($this->paramsEmission[$waste->getType()], current([$waste->getSubType()]))) / 1000 * $waste->getMass();
        } else {
            return $this->paramsEmission[$waste->getType()] / 1000 * $waste->getMass();
        }
    }

    // Retourne le CO2 émis dû au traîtement du déchet.
    public function getCO2Issued(): float
    {
        return $this->CO2Issued;
    }

    // Permet de tester si la méthode de traîtement est disponible suivant le déchet.
    public function canHandle(Waste $waste): bool
    {

        // Si le type de déchet est égal à ce que le recyleur peut traiter.
        if ($this->type == $waste->getType()) {

            // Si le sous type du déchet est égal à ce que le recyleur peut traiter sinon application de la verification de la place disponible dans le recycleur.

            if ($waste->getSubType() && $waste->getSubType() !== "") {
                for ($i = 0; $i < count($this->subType); $i++) {

                    if ($this->subType[$i] == $waste->getSubType()) {
                        if ($this->capacityMax > 0) {
                            return true;
                        }
                        return false;
                    }
                }
                return false;
            } else {

                if ($this->capacityMax > 0) {
                    return true;
                }
                return false;
            }
        }
        return false;
    }

    // Recyclage du déchet et création d'un sous déchet quand la masse du déchet à recycler et supperieur à la masse de traîtement possible.
    public function handle(Waste $waste, $createWaste): void
    {
        if ($this->canHandle($waste)) {
            if (($waste->getMass() - $this->capacityMax) > 0) {

                $createWaste(($waste->getMass() - $this->capacityMax), $waste->getType(), $waste->getSubType(), true);
                $waste->setMass($this->capacityMax);
            }

            $waste->destroy();
            $waste->setCO2issued($this->getCO2AfterTreatment($waste));
            $waste->setHandlerType(str_replace('Handler', '', explode('\\', get_class($this))[2]));
            $this->capacityMax -= $waste->getMass();
            $this->wasteHandled++;
        }
    }
}
