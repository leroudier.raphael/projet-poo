<?php

namespace App\Services;

use App\Business\Waste;

interface SortingCenterHandlerInterface
{
    function canHandle(Waste $waste): bool;
    function handle(Waste $waste, array $wastesHandlers, $createWaste): void;
}
