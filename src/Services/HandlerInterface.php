<?php
namespace App\Services;

use App\Business\Waste;

interface HandlerInterface
{
    function getCO2AfterTreatment(Waste $waste) : float;
    function getCO2Issued() : float;
    function canHandle(Waste $waste) : bool;
    function handle(Waste $waste, $createWaste) : void;
}
