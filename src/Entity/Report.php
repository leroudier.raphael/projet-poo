<?php

namespace App\Entity;
use App\Business\Waste;
use App\Repository\ReportRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ReportRepository::class)
 */
class Report
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $state;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbWastes;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbTreatedWastes;

    /**
     * @ORM\Column(type="float")
     */
    private $totalCO2Issued;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $wastes;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nbSubWastes;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nbTreatedSubWastes;



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(string $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getNbWastes(): ?int
    {
        return $this->nbWastes;
    }

    public function setNbWastes(int $nbWastes): self
    {
        $this->nbWastes = $nbWastes;

        return $this;
    }

    public function getNbTreatedWastes(): ?int
    {
        return $this->nbTreatedWastes;
    }

    public function setNbTreatedWastes(int $nbTreatedWastes): self
    {
        $this->nbTreatedWastes = $nbTreatedWastes;

        return $this;
    }

    public function getNbSubWastes(): ?int
    {
        return $this->nbSubWastes;
    }

    public function setNbSubWastes(?int $nbSubWastes): self
    {
        $this->nbSubWastes = $nbSubWastes;

        return $this;
    }

    public function getNbTreatedSubWastes(): ?int
    {
        return $this->nbTreatedSubWastes;
    }

    public function setNbTreatedSubWastes(?int $nbTreatedSubWastes): self
    {
        $this->nbTreatedSubWastes = $nbTreatedSubWastes;

        return $this;
    }

    public function getTotalCO2Issued(): ?float
    {
        return $this->totalCO2Issued;
    }

    public function setTotalCO2Issued(float $totalCO2Issued): self
    {
        $this->totalCO2Issued = $totalCO2Issued;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

  
    public function getWastes()
    {
        return $this->wastes;
    }

    public function addWaste(Waste $waste): self
    {
    
            $this->wastes[] = $waste;
            $waste->setReport($this);
        

        return $this;
    }

    public function removeWaste(Waste $waste): self
    {
    unset($this->wastes[array_search($waste, $this->wastes)]);


        return $this;
    }

}
