<?php

namespace App\Business;

use App\Entity\Report;
use App\Repository\ReportRepository;
use Doctrine\ORM\EntityManagerInterface;

interface ReportManagerInterface
{
    function create(WasteManager $wasteManager): void;
    function persist(EntityManagerInterface $manager): int;
    function get(int $id, ReportRepository $repo): Report;
    function getChartResume(int $id, ReportRepository $repo): array;
    function getAll(ReportRepository $repo): array;
}
