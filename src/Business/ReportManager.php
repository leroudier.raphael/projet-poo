<?php

namespace App\Business;

use App\Entity\Report;
use App\Repository\ReportRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;

class ReportManager implements ReportManagerInterface
{
    private $report;

    function __construct()
    {
    }

    public function create($wasteManager): void
    {

        $this->report = new Report();
        $this->report
            ->setState('success')
            ->setNbWastes($wasteManager->getTotalWastes())
            ->setNbSubWastes($wasteManager->getTotalSubWastes())
            ->setNbTreatedWastes($wasteManager->getTotalWastesTreated())
            ->setNbTreatedSubWastes($wasteManager->getTotalSubWastesTreated())
            ->setTotalCO2Issued($wasteManager->getTotalCO2Issued())
            ->setDate(new DateTime('NOW'));

        $wastes = $wasteManager->getWastesObject();

        for ($i = 0; $i < count($wastes); $i++) {
            $this->report->addWaste($wastes[$i]);
        }
    }

    public function persist(EntityManagerInterface $manager): int
    {
        $manager->persist($this->report);
        $manager->flush();
        return $this->report->getId();
    }

    public function get(int $id, ReportRepository $repo): Report
    {
        return $repo->find($id);
    }

    public function getChartResume(int $id, ReportRepository $repo): array
    {
        $report = $repo->find($id);
        $wastes = $report->getWastes();

        $buffer = array();
        for ($i = 0; $i < count($wastes); $i++) {

            if (!isset($buffer[$wastes[$i]->getHandlerType()])) {
                $buffer[$wastes[$i]->getHandlerType()] = 1;
            } else {
                $buffer[$wastes[$i]->getHandlerType()] += 1;
            }
        }
        $handlersByWastes = array(["Handler", "Wastes"]);
        for ($i = 0; $i < count($buffer); $i++) {

            array_push($handlersByWastes, [array_keys($buffer)[$i], array_values($buffer)[$i]]);
        }

        $buffer = array();
        for ($i = 0; $i < count($wastes); $i++) {

            if (!isset($buffer[$wastes[$i]->getHandlerType()])) {
                $buffer[$wastes[$i]->getHandlerType()] = $wastes[$i]->getCO2Issued();
            } else {
                $buffer[$wastes[$i]->getHandlerType()] += $wastes[$i]->getCO2Issued();
            }
        }
        $handlersCO2Issued = array(["Handler", "Wastes"]);
        for ($i = 0; $i < count($buffer); $i++) {

            array_push($handlersCO2Issued, [array_keys($buffer)[$i], array_values($buffer)[$i]]);
        }

        return [$handlersByWastes, $handlersCO2Issued];
    }

    public function getAll(ReportRepository $repo): array
    {
        return $repo->findAll();
    }
}
