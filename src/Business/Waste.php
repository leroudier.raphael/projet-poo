<?php

namespace App\Business;
use App\Entity\Report;

class Waste implements WasteInterface{
  
    private $id;
    private $mass;
    private $type;
    private $subType;
    private $CO2Issued;
    private $destroyed;
    private $handlerType;
    private $report;
    private $subWaste;

    function __construct(float $mass, string $type, ?string $subType = NULL, ?bool $subWaste = false)
    {
        $this->mass = $mass;
        $this->type = $type;
        $this->subType = $subType;
        $this->destroyed = false;
        $this->subWaste = $subWaste;
        $this->handlerType = "Untreated";
        $this->formatType();
        
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMass(): ?int
    {
        return $this->mass;
    }

    public function setMass(int $mass): self
    {
        $this->mass = $mass;

        return $this;
    }
    public function getSubType(): ?string
    {
        return $this->subType;
    }
    private function formatType() : void 
    {
        if($this->subType)
        {
            if($this->type[strlen($this->type)-1] == "s")
            {
                $this->type = substr_replace($this->type,"",-1);
            }
        }
    }
    public function setSubType(?string $subType): self
    {
        $this->subType = $subType;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

 

   
    public function getCO2Issued(): ?float
    {
        return $this->CO2Issued;
    }

    public function setCO2Issued(?float $CO2Issued): self
    {
        $this->CO2Issued = $CO2Issued;

        return $this;
    }

    public function isDestroyed(): ?bool
    {
        return $this->destroyed;
    }

    public function destroy(): void
    {
        $this->destroyed = true;

    }



    public function getHandlerType(): ?string
    {
        return $this->handlerType;
    }

    public function setHandlerType(?string $handlerType): self
    {
        $this->handlerType = $handlerType;

        return $this;
    }

    public function getReport(): Report
    {
        return $this->report;
    }

    public function setReport(Report $report): self
    {
        $this->report = $report;

        return $this;
    }
    public function isSubWaste(): ?bool
    {
        return $this->subWaste;
    }

    public function setSubWaste(bool $subWaste): self
    {
        $this->subWaste = $subWaste;

        return $this;
    }
}