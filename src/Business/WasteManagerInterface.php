<?php

namespace App\Business;

interface WasteManagerInterface
{
    function createWastes(): void;
    function createWaste(float $mass, string $type, ?string $subType = null, ?bool $subWaste = false): void;
    function createHandlers(): void;
    function sortingWastes(): void;
    function getTotalWastes(): int;
    function getTotalWastesTreated(): int;
    function getTotalCO2Issued(): float;
    function getWastesObject(): array;
    function getWastesArray(): array;
}
