<?php

namespace App\Business;

use App\Entity\Report;

interface WasteInterface
{
    function getId(): ?int;
    function getMass(): ?int;
    function setMass(int $mass): self;
    function getType(): ?string;
    function setType(string $type): self;
    function getCO2Issued(): ?float;
    function setCO2Issued(?float $CO2Issued): self;
    function isDestroyed(): ?bool;
    function destroy(): void;
    function getHandlerType(): ?string;
    function setHandlerType(?string $handlerType): self;
    function getReport(): Report;
    function setReport(Report $report): self;   
    function isSubWaste(): ?bool;
    function setSubWaste(bool $subWaste): self;
}
