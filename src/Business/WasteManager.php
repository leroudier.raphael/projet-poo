<?php

namespace App\Business;

use App\Business\Waste;
use App\Services\ComposterHandler;
use App\Services\IncineratorHandler;
use App\Services\RecyclingCenterHandler;
use App\Services\SortingCenterHandler;
use App\Business\WasteManagerInterface;

class WasteManager implements WasteManagerInterface
{
    private array $input;
    private array $incineratorEmission;
    private array $recyclingEmission;
    private array $composterEmission;
    private array $wastes = array();
    private array $sortingCenters = array();
    private array $handlers = array();

    function __construct(array $input, array $emissionParameter)
    {
        $this->input = $input;
        $this->incineratorEmission = $emissionParameter[0];
        $this->recyclingEmission = $emissionParameter[1];
        $this->composterEmission = $emissionParameter[2];
    }

    public function createWaste(float $mass, string $type, ?string $subType = null, ?bool $subWaste = false): void
    {
        array_push($this->wastes, new Waste($mass, $type, $subType, $subWaste));
    }

    // Création des déchets.
    public function createWastes(): void
    {
        for ($i = 0; $i < count($this->input["quartiers"]); $i++) {

            for ($ii = 1; $ii < count($this->input["quartiers"][$i]); $ii++) {

                if (is_array(array_values($this->input["quartiers"][$i])[$ii])) {
                    for ($iii = 0; $iii < count(array_values($this->input["quartiers"][$i])[$ii]); $iii++) {

                        $mass = array_values(array_values($this->input["quartiers"][$i])[$ii])[$iii];
                        $type = array_keys($this->input["quartiers"][$i])[$ii];
                        $subType = array_keys(array_values($this->input["quartiers"][$i])[$ii])[$iii];

                        $this->createWaste($mass, $type, $subType);
                    }
                } else {
                    $mass = array_values($this->input["quartiers"][$i])[$ii];
                    $type = array_keys($this->input["quartiers"][$i])[$ii];

                    $this->createWaste($mass, $type);
                }
            }
        }
    }

    // Création des services de traitements des déchets.
    public function createHandlers(): void
    {

        for ($i = 0; $i < count($this->input["services"]); $i++) {

            switch ($this->input["services"][$i]["type"]) {
                case "centreTri":

                    $capacityMax = array_values($this->input["services"][$i])[1];
                    array_push($this->sortingCenters, new SortingCenterHandler($capacityMax));
                    break;

                case "incinerateur":

                    $ovenLineMax = array_values($this->input["services"][$i])[1];
                    $capacityLineMax =  array_values($this->input["services"][$i])[2];
                    array_push($this->handlers, new IncineratorHandler($ovenLineMax, $capacityLineMax, $this->incineratorEmission));
                    break;

                case "recyclagePlastique":
                case "recyclagePapier":
                case "recyclageVerre":
                case "recyclageMetaux":

                    $capacityMax = NULL;
                    $type = NULL;
                    $subType = array();
                    $deposit = false;

                    for ($ii = 0; $ii < count($this->input["services"][$i]); $ii++) {
                        if (array_keys($this->input["services"][$i])[$ii] == "capacite") {
                            $capacityMax = array_values($this->input["services"][$i])[$ii];
                        }
                    }
                    for ($ii = 0; $ii < count($this->input["services"][$i]); $ii++) {
                        if (array_keys($this->input["services"][$i])[$ii] == "type") {
                            $type = array_values($this->input["services"][$i])[$ii];
                        }
                    }
                    for ($ii = 0; $ii < count($this->input["services"][$i]); $ii++) {
                        if (array_keys($this->input["services"][$i])[$ii] == "plastiques") {
                            $subType = array_values($this->input["services"][$i])[$ii];
                        }
                    }
                    for ($ii = 0; $ii < count($this->input["services"][$i]); $ii++) {
                        if (array_keys($this->input["services"][$i])[$ii] == "consigne") {
                            $deposit = array_values($this->input["services"][$i])[$ii];
                        }
                    }

                    array_push($this->handlers, new RecyclingCenterHandler($capacityMax, $type, $subType, $deposit, $this->recyclingEmission));
                    break;

                case "composteur":

                    $capacityMax = $this->input["services"][$i]["capacite"];
                    $family = $this->input["services"][$i]["foyers"];

                    array_push($this->handlers, new ComposterHandler($capacityMax, $family, $this->composterEmission));
                    break;

                default:
                    break;
            }
        }
    }

    // Tri des déchets via les services de tris disponibles et envoi des déchets restant à l'incinerateur si centre de tri complet.
    public function sortingWastes(): void
    {
        for ($i = 0; $i < count($this->wastes); $i++) {

            for ($ii = 0; $ii < count($this->sortingCenters); $ii++) {

                if ($this->sortingCenters[$ii]->canHandle($this->wastes[$i]) && !$this->wastes[$i]->isDestroyed()) {
                    $this->sortingCenters[$ii]->handle($this->wastes[$i], $this->handlers, function (float $mass, string $type, ?string $subType = null, ?bool $subWaste = false) {
                        $this->createWaste($mass,  $type,  $subType, $subWaste);
                    });
                }
            }
        }
        for ($i = 0; $i < count($this->wastes); $i++) {

            for ($ii = 0; $ii < count($this->handlers); $ii++) {

                if ($this->handlers[$ii] instanceof IncineratorHandler) {

                    if ($this->handlers[$ii]->canHandle($this->wastes[$i]) && !$this->wastes[$i]->isDestroyed()) {
                        $this->handlers[$ii]->handle($this->wastes[$i], function (float $mass, string $type, ?string $subType = null, ?bool $subWaste = false) {
                            $this->createWaste($mass,  $type,  $subType, $subWaste);
                        });
                    }
                }
            }
        }
    }

    public function getTotalWastes(): int
    {
        $totalWastes = 0;

        for ($i = 0; $i < count($this->wastes); $i++) {

            if (!$this->wastes[$i]->isSubWaste()) {
                $totalWastes++;
            }
        }
        return $totalWastes;
    }

    public function getTotalSubWastes(): int
    {
        $totalSubWastes = 0;

        for ($i = 0; $i < count($this->wastes); $i++) {

            if ($this->wastes[$i]->isSubWaste()) {
                $totalSubWastes++;
            }
        }
        return $totalSubWastes;
    }

    public function getTotalWastesTreated(): int
    {
        $totalTreatedWastes = 0;

        for ($i = 0; $i < count($this->wastes); $i++) {

            if ($this->wastes[$i]->isDestroyed() && !$this->wastes[$i]->isSubWaste()) {
                $totalTreatedWastes++;
            }
        }

        return $totalTreatedWastes;
    }

    public function getTotalSubWastesTreated(): int
    {
        $totalTreatedSubWastes = 0;

        for ($i = 0; $i < count($this->wastes); $i++) {

            if ($this->wastes[$i]->isDestroyed() && $this->wastes[$i]->isSubWaste()) {
                $totalTreatedSubWastes++;
            }
        }

        return $totalTreatedSubWastes;
    }

    public function getTotalCO2Issued(): float
    {
        $totalCO2Issued = 0;

        for ($i = 0; $i < count($this->wastes); $i++) {

            if ($this->wastes[$i]->isDestroyed()) {
                $totalCO2Issued += $this->wastes[$i]->getCO2Issued();
            }
        }

        return $totalCO2Issued;
    }

    public function getWastesObject(): array
    {
        return $this->wastes;
    }

    public function getWastesArray(): array
    {
        $wastes = array();
        for ($i = 0; $i < count($this->wastes); $i++) {

            array_push(
                $wastes,
                array(
                    'subWaste' => $this->wastes[$i]->isSubWaste(),
                    'type' => $this->wastes[$i]->getType(),
                    'subType' => $this->wastes[$i]->getSubType(),
                    'destroyed' => $this->wastes[$i]->isDestroyed(),
                    'CO2Issued' => $this->wastes[$i]->getCO2Issued(),
                    'mass' => $this->wastes[$i]->getMass(),
                    'handlerName' => $this->wastes[$i]->getHandlerType()
                )
            );
        }
        return $wastes;
    }
}
