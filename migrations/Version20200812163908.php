<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200812163908 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE report (id INT AUTO_INCREMENT NOT NULL, state VARCHAR(255) NOT NULL, nb_wastes INT NOT NULL, nb_treated_wastes INT NOT NULL, total_co2_issued DOUBLE PRECISION NOT NULL, date DATETIME NOT NULL, wastes LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', nb_sub_wastes INT DEFAULT NULL, nb_treated_sub_wastes INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE report');
    }
}
